<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReposTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('repos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('project');
            $table->string('master_ip')->nullable();
            $table->string('master_user')->nullable();
            $table->string('master_path')->nullable();
            $table->string('qa_ip')->nullable();
            $table->string('qa_user')->nullable();
            $table->string('qa_path')->nullable();
            $table->string('dev_ip')->nullable();
            $table->string('dev_user')->nullable();
            $table->string('dev_path')->nullable();
            $table->string('ip_add')->nullable();
            $table->string('drc_user')->nullable();
            $table->string('drc_path')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('repos');
    }
}
