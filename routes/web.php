<?php

Route::group(['namespace'=>'Bryanjack\Git\Controllers'], function(){
	Route::post('/git', 'GitController@parse');
	Route::get('chatyuk', 'GitController@chat');
	Route::get('/bryanawesome', 'GitController@date');
});
