<?php

namespace Bryanjack\Git\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Bryanjack\Git\Model\Repo;
use Longman\TelegramBot\Request as TelReq;

class GitController extends Controller
{
    public function parse(Request $request)
	{
		//check token
		if($request->header('X-Gitlab-Token') == 'bryan_awesome'){
			$body = json_decode($request->getContent(), true);
			$method = $body['object_kind'];
			$project = $body['project']['name'];

			if($method == "push"){
				$trigger = "define";
				$branch = explode('/',$body['ref']);
				$branch = $branch[2];
				$user = $body['user_username'];
			} elseif ($method == "merge_request") {
				$trigger = "define";
				$branch = $body['object_attributes']['target_branch'];
				$user = $body['user']['username'];
			}else{
				$trigger = "Unregistered Method";
			}
		}else{
			file_put_contents('command.sh', "Anda tidak diijinkan");
			return "You're not authorized";
		}
		
		if($trigger == 'define'){
			$header = $user.' '.$method.' : '.$branch.'@'.$project;
			$this->bashGen($project,$branch);
			$command = shell_exec("./command.sh 2>&1");
		}else{
			$header = 'Undefine trigger > '.$method;
		}
		
		$date = date_create();
		$date = date_format($date, 'Y-m-d H:i:s');
		
		$txt = "<pre>===================================================================================================<br>";
		$txt .= '<b>'.$date.' - '.$header."</b><br>";
		$txt .= $command;
		$txt .= "</pre>".file_get_contents('logwow.html');

		$msg = $date.PHP_EOL.$header.PHP_EOL;
		$msg .= '```'.
		$command
		.'```';
		
		$this->chat($msg);
		file_put_contents('log.html', $txt);
	}

	public function date(){
		$date = date_create();
		$date = date_format($date, 'Y-m-d H:i:s');
		return $date;
	}
	
	public function bashGen($project,$branch)
	{
		$repos = Repo::where('project', $project)->first();
		$repos = $repos->toArray();
		// dd($repos['master_ip']);

		$content = 'HOST="'.$repos[$branch."_user"].'@'.$repos[$branch."_ip"].'"'."\n";
		$content .= 'ssh -o "StrictHostKeyChecking=no" $HOST '."'\n";
		$content .= 'unset GIT_DIR'."\n";
		$content .= 'cd '.$repos[$branch."_path"]."\n";
		$content .= "git pull origin ".$branch."'\n";
		file_put_contents('command.sh',$content);
	}

	public function chat($text = 'test')
	{
		$bot_api_key  = env('bot_api_key');
		$bot_username = env('bot_username');
		$telegram = new \Longman\TelegramBot\Telegram($bot_api_key, $bot_username);
		$result = TelReq::sendMessage([
			'chat_id' => env('chat_id'),
			'parse_mode' => 'MARKDOWN',
			'text'    => $text,
		]);
		return $result;
	}
}
